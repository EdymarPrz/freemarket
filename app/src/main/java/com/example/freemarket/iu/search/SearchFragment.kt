package com.example.freemarket.iu.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.example.freemarket.R
import com.example.freemarket.iu.afterTextChanged
import com.example.freemarket.iu.getErrorMessageWithIcon
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : Fragment() {

    companion object {
        const val SEARCH_PRODUCT = "search_product_key"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    fun initView(){
        searchInput.afterTextChanged(::validateIsNotEmpty)
        nextButton.setOnClickListener {
            if (searchInput.text.toString().isNotEmpty()){
                val args = Bundle()
                args.putString(SEARCH_PRODUCT, searchInput.text.toString())
                findNavController(this@SearchFragment).navigate(
                    R.id.action_searchFragment_to_listProductFragment,
                    args
                )
            } else {
                showError(true)
            }
        }
    }

    private fun validateIsNotEmpty(chars: CharSequence){
        if(chars.toString().isNotEmpty() && chars.toString().isNotBlank()){
            showError(false)
        } else {
            showError(true)
        }
    }

    private fun showError(show: Boolean) {
        if(show){
            searchInputLayout.error = getErrorMessageWithIcon(constraintLayout.context, R.string.search_input_is_empty)
        } else {
            searchInputLayout.error = null
        }
    }
}
