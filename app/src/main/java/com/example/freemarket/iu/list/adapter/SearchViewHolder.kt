package com.example.freemarket.iu.list.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.freemarket.network.Product
import kotlinx.android.synthetic.main.item_product.view.*

class SearchViewHolder(private val view: View) : RecyclerView.ViewHolder(view){

    fun bindItem(productResult: Product) = with(view) {
        title.text = productResult.title
        price.text = productResult.price.toString()
        //view.imageProduct.loadImageURL(productResult.thumbnail.replaceHttpToHttps())
        imageProduct.visibility = View.VISIBLE
        Glide.with(this).load(productResult.thumbnail).into(imageProduct)
    }
}
