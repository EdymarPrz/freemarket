package com.example.freemarket.iu.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.freemarket.network.SearchResponse
import com.example.freemarket.network.api.MercadoLibreService


class ListProductViewModel : ViewModel() {

    private val _products = MutableLiveData<SearchResponse>()
    val products: LiveData<SearchResponse>
        get() = _products
    val loading = MutableLiveData<Boolean>()

    var mercadoLibreService = MercadoLibreService()

    fun getProducts(result: String){
        _products.postValue(mercadoLibreService.getListProduct(result))
    }

}