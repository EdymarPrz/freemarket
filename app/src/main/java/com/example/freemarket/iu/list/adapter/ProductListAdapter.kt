package com.example.freemarket.iu.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.freemarket.R
import com.example.freemarket.network.Product

class ProductListAdapter() : RecyclerView.Adapter<SearchViewHolder>() {

    private var items: List<Product> = arrayListOf()

    fun setItems(items: List<Product>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_product, parent, false)
        return SearchViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

}