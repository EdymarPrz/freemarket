package com.example.freemarket.iu.list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.freemarket.R
import com.example.freemarket.iu.list.adapter.ProductListAdapter
import kotlinx.android.synthetic.main.fragment_list_product.*

class ListProductFragment : Fragment() {

    private lateinit var viewModel: ListProductViewModel
    private val productAdapter = ProductListAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_product, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(ListProductViewModel::class.java)

        initRecyclerView()
    }

    private fun setProductObserver() {
        viewModel.products.observe(viewLifecycleOwner, Observer { response ->
            /*when (response.status) {
                LOADING -> showLoading()
                SUCCESS -> setResponse(response.data)
                SERVER_ERROR -> showError(PROBLEM)
                CONNECTION_ERROR -> showError(NETWORK)
            }*/
        })

        viewModel.loading.observe(this, Observer { isLoading ->
            isLoading?.let {
                animationLoading.visibility = if(it) View.VISIBLE else View.GONE
            }
        })
    }

    private fun initRecyclerView() {
        recyclerView.adapter = productAdapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun goToProductList() {

    }

    private fun showList(){

    }

}
