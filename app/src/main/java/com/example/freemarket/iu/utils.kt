package com.example.freemarket.iu

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextWatcher
import android.text.style.ImageSpan
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.freemarket.R

fun EditText.afterTextChanged(afterTextChanged: (CharSequence) -> Unit) {
        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                afterTextChanged.invoke(s ?: "")
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    fun getProgressDrawable(context: Context): CircularProgressDrawable {
        return CircularProgressDrawable(context).apply {
            strokeWidth = 10f
            centerRadius = 50f
            start()
        }
    }

    fun ImageView.loadImage(url: String?, progressDrawable: CircularProgressDrawable) {
        val options = RequestOptions()
            .placeholder(progressDrawable)
            .error(R.mipmap.ic_launcher_round)
        Glide.with(this.context)
            .setDefaultRequestOptions(options)
            .load(url)
            .into(this)
    }

    fun Fragment.getErrorMessageWithIcon(context: Context, stringResId: Int): SpannableString {

        val errorDrawable = AppCompatResources.getDrawable(context, R.drawable.ic_error_message)
            ?: ColorDrawable(Color.TRANSPARENT)
        errorDrawable.apply {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
        }

        val errorString = "  " + getString(stringResId)

        return SpannableString(errorString).apply {
            setSpan(
                ImageSpan(errorDrawable, ImageSpan.ALIGN_BOTTOM),
                0, 1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE)
        }
}