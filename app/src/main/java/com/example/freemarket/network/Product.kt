package com.example.freemarket.network

import com.google.gson.annotations.SerializedName

data class Product(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("site_id")
    val site: String,
    @SerializedName("thumbnail")
    val thumbnail: String,
    @SerializedName("currency_id")
    val currency: String,
    @SerializedName("condition")
    val condition: String,
    @SerializedName("available_quantity")
    val available: Int,
    @SerializedName("price")
    val price: Float,
    @SerializedName("accepts_mercadopago")
    val mercado_pago: Boolean
)


