package com.example.freemarket.network.api

import com.example.freemarket.network.SearchResponse
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MercadoLibreService {

    val urlApi = "https://api.mercadolibre.com/sites/MLA/"
    private val api: MercadoLibreApi

    init {
        api = Retrofit.Builder()
            .baseUrl(urlApi)
            .client(OkHttpClient.Builder().build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(MercadoLibreApi::class.java)
    }

    fun getListProduct(product: String) : SearchResponse {
        return api.searchItem(product)
    }

}