package com.example.freemarket.network.api

import com.example.freemarket.network.SearchResponse
import retrofit2.http.GET
import retrofit2.http.Query


interface MercadoLibreApi {

    @GET("search")
    fun searchItem(@Query("q") product: String): SearchResponse

}