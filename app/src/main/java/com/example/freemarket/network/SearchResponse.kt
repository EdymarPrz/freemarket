package com.example.freemarket.network

data class SearchResponse (val query: String,
                           val results: List<Product>)